<?php
namespace skmnt\skyadmin\shop;

use yii\base\BootstrapInterface;
use yii\base\Application;
use common\models\Dictionary;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if (get_class($app) == 'yii\web\Application') {
            $app->on(Application::EVENT_BEFORE_REQUEST, function() {
                \Yii::$app->urlManager->addRules([
                    'admin/dictionary/index/<type:[a-z-]+>' => 'skyadmin-shop/dictionary/index',
                    'admin/dictionary/create/<type>'        => 'skyadmin-shop/dictionary/create',
                    'admin/dictionary/update/<id>'          => 'skyadmin-shop/dictionary/update',
                    'admin/dictionary/sort-action'          => 'skyadmin-shop/dictionary/sort-action',
                    'admin/dictionary/bulk-action'          => 'skyadmin-shop/dictionary/sort-action',
                    'admin/shop-utils'                      => 'skyadmin-shop/utils',
                ]);
            });
            \Yii::$app->setModule('skyadmin-shop', ['class' => __NAMESPACE__ . '\Module']);
            $this->registerAdminMenu();
        }
    }

    private function registerAdminMenu()
    {
        \Yii::$app->params['skyadmin']['adminMenu'][] = [
            'role' => 'ADMIN',
            'items' => [
                ['label' => 'Магазин', 'options' => ['class' => 'header']],
                ['label' => 'Товары' . $this->singleTableStat('product'), 'icon' => 'cubes', 'url' => ['/skyadmin-shop/product/index'], 'encode' => false],
                ['label' => 'Заказы' . $this->singleTableStat('order'), 'icon' => 'folder', 'url' => ['/skyadmin-shop/order/index'], 'encode' => false,],
                [
                    'label' => 'Справочники', 'icon' => 'book',
                    'items' => $this->dictionaries(),
                ],
                [
                    'label' => 'SEO', 'icon' => 'book',
                    'items' => $this->seo(),
                ],
            ],
        ];
        /**
         * Extra support menu items.
         */
        // \Yii::$app->params['skyadmin']['supportMenu'] = [
        //     ['label' => 'Очистить все справочники', 'icon' => 'cogs', 'url' => ['/skyadmin-shop/utils/dictionary-clean'], 'encode' => false],
        //     ['label' => 'Очистить все товары', 'icon' => 'cogs', 'url' => ['/skyadmin-shop/utils/product-clean'], 'encode' => false],
        // ];
    }

    private function dictionaries()
    {
        $dictionaryCounters = \yii\helpers\ArrayHelper::map(
            \Yii::$app->getDb()->createCommand('select type, count(*) as cnt from dictionary group by type')->queryAll(),
            'type', 'cnt'
        );
        array_walk($dictionaryCounters, function(&$i){
            if ($i > 0) $i = ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $i . '</span>';
        });
        $items = [];
        foreach (Dictionary::productTypes() as $id => $type) {
            $items[] = [
                'encode' => false,
                'label' => (@Dictionary::$controllerTitles[$id] ?: 'n/a') . @$dictionaryCounters[$id],
                'icon' => @Dictionary::$controllerIcons[$id] ?: 'file',
                'url' => ['/skyadmin-shop/dictionary/index', 'type' => $type],
            ];
        }
        return $items;
    }

    private function seo()
    {
        $dictionaryCounters = \yii\helpers\ArrayHelper::map(
            \Yii::$app->getDb()->createCommand('select type, count(*) as cnt from dictionary group by type')->queryAll(),
            'type', 'cnt'
        );
        array_walk($dictionaryCounters, function(&$i){
            if ($i > 0) $i = ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $i . '</span>';
        });
        $items = [];
        foreach (Dictionary::siteTypes() as $id => $type) {
            $items[] = [
                'encode' => false,
                'label' => (@Dictionary::$controllerTitles[$id] ?: 'n/a') . @$dictionaryCounters[$id],
                'icon' => @Dictionary::$controllerIcons[$id] ?: 'file',
                'url' => ['/skyadmin-shop/dictionary/index', 'type' => $type],
            ];
        }
        return $items;
    }

    private function singleTableStat($tableName)
    {
        $cnt = \Yii::$app->getDb()->createCommand('select count(id) as cnt from ' . "`{$tableName}`")->queryScalar();
        if ($cnt > 0) return ' <span class="pull-right badge" style="margin-top: 2px; font-weight: normal;">' . $cnt . '</span>';
    }
}
