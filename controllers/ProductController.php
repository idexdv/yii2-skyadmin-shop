<?php

namespace skmnt\skyadmin\shop\controllers;

use Yii;
use yii\web\Controller;
use common\models\Product;
use skmnt\skyadmin\shop\models\ProductSearch;

class ProductController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;
    use \skmnt\skyadmin\shop\components\DataExportTrait;

    private $type;
    private $baseClass = Product::class;
    private $searchClass = ProductSearch::class;
    private $rootController = '/skyadmin-shop/product';

    public function actionIndex($download = false)
    {
        $searchModel = new $this->searchClass();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        if ($download) {
            $this->downloadData($dataProvider, $searchModel);
            return $this->redirect(Url::current(['download' => null]));
        }

        return $this->render('index', [
            'title' => 'Товары',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        return $this->actionUpdate(null, true);
    }

    public function actionUpdate($id, $__createMode = false)
    {
        if ($id === null && $__createMode === true) {
            $model = new $this->baseClass([
                'active' => \Yii::$app->st->defaultsNewProductActive_string('1'),
            ]);
        } else {
            $model = $this->findModel($id);
        }

        if (method_exists($model, 'propSave')) $model->propSave();

        if ($model->load(\Yii::$app->request->post()) && $model->preProcess() && $model->save()) {
            if (!isset($_POST['exitmode']))
                return $this->redirect(["{$this->rootController}/update", 'id' => $model->id]);    
            else {
                return $this->redirect(["{$this->rootController}/index"]);
            }
        } else {
            return $this->render($__createMode ? 'create' : 'update', [
                'model' => $model,
                'indexTitle' => 'Товары',
                'indexAction' => ["{$this->rootController}/index"],
                'indexSearchId' => (new $this->searchClass())->formName(),
            ]);
        }
    }

    public function actionBulkEdit()
    {
        $ids = Yii::$app->request->post('ids');
        $action = Yii::$app->request->post('action');

        $bulkForm = \yii\base\DynamicModel::validateData(compact('ids', 'action'), [
            ['ids', 'string', 'max' => 5000],
            ['action', 'string', 'max' => 50],
            ['ids', 'required', 'message' => 'Укажите список товаров (id-коды через запятую)']
        ]);

        if(!$bulkForm->hasErrors()) {
            return $this->render('bulk-edit', [
                'ids' => $bulkForm->ids,
                'model' => new Product,
            ]);
        }
        else {
            Yii::$app->session->setFlash('error', 'Ошибка в форме групповой обработки.<br><br>' . \yii\helpers\Html::errorSummary($bulkForm));
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBulkEditSave()
    {
        $model = new Product;

        if ($model->load(Yii::$app->request->post()))
        {
            $attributes = [];
            foreach ($model->attributes as $key => $value) {
                $value = is_array($value) ? array_filter($value) : $value;
                if (!empty($value)) $attributes[$key] = $value;
            }

            if(empty($attributes)) {
                Yii::$app->session->setFlash('warning', 'Групповая обработка не выполнена, не заданы свойства для изменения.');
                return $this->redirect(['index']);
            }

            $_ids = explode(',', Yii::$app->request->post('ids')) ?: [];
            $_ids = array_unique(array_filter($_ids, 'trim'));

            if(empty($_ids)) {
                Yii::$app->session->setFlash('warning', 'Групповая обработка не выполнена, не заданы товары для изменения.');
                return $this->redirect(['index']);
            }

            foreach ($attributes as $key => $value) {
                Yii::$app->session->addFlash('info', 'Изменено: ' . "{$key} -> " . print_r($value, true));
            }

            foreach ($_ids as $pid) {
                $product = Product::findOne($pid);

                $newAttributes = array_replace_recursive($product->attributes, $attributes);
                array_walk_recursive($newAttributes, function(&$el, $key) {
                    if ($el == '!') $el = null;
                });
                $newAttributes['prop'] = array_filter($newAttributes['prop']);

                $product->attributes = $newAttributes;

                if(!$product->save()) {
                    throw new \yii\web\ServerErrorHttpException('Ошибка груп.обработки, товар #' . $pid);
                }
            }

            Yii::$app->session->setFlash('success', 'Групповая обработка завершена.');
            setcookie('group-id-list', null, time() - 1, '/');
        }

        return $this->redirect(['index']);
    }
}
