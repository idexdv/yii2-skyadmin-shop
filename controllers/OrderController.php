<?php

namespace skmnt\skyadmin\shop\controllers;

use Yii;
use yii\web\Controller;
use common\models\Order;
use skmnt\skyadmin\shop\models\OrderSearch;

class OrderController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;

    private $type;
    private $baseClass = Order::class;
    private $searchClass = OrderSearch::class;
    private $rootController = '/skyadmin-shop/order';

    public function actionIndex()
    {
        $searchModel = new $this->searchClass();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'title' => 'Заказы',
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        return $this->actionUpdate(null, true);
    }

    public function actionUpdate($id, $__createMode = false)
    {
        if ($id === null && $__createMode === true) {
            $model = new $this->baseClass([
                'status' => $this->baseClass::S_NEW,
                'manager_id' => \Yii::$app->user->identity->id,
            ]);
        } else {
            $model = $this->findModel($id);
        }

        if ($model->load(\Yii::$app->request->post()) && $model->preProcess() && $model->save()) {
            if (!isset($_POST['exitmode']))
                return $this->redirect(["{$this->rootController}/update", 'id' => $model->id]);
            else {
                return $this->redirect(["{$this->rootController}/index"]);
            }
        } else {
            return $this->render($__createMode ? 'create' : 'update', [
                'model' => $model,
                'indexTitle' => 'Заказы',
                'indexAction' => ["{$this->rootController}/index"],
                'indexSearchId' => (new $this->searchClass())->formName(),
                'isCreateMode' => $__createMode,
            ]);
        }
    }

    public function actionProductFeed($q)
    {
        $rows = \common\models\Product::find()->select('id,title')->where(['like', 'title', $q])->indexBy('id')->limit(10)->asArray()->all();
        $items = [];
        foreach ($rows as $key => $item) {
            $items[$item['id']] = [
                'id' => $item['id'],
                'value' => html_entity_decode($item['title']),
            ];
        }
        return $this->asJson($items);
    }

    public function actionAddNewItem($oid)
    {
        $order = $this->findModel($oid);
        if ($product = \common\models\Product::findOne($_POST['product_id'])) {
            $order->addItem([
                'product' => $product,
                'c' => $_POST['product_qty'],
            ]);
        }
        return $this->redirect(["{$this->rootController}/update", 'id' => $oid, '#' => 'items']);
    }

    public function actionUpdateItemCount($oid)
    {
        $order = $this->findModel($oid);
        return $this->asJson(['result' => $order->updateItemCount($_POST['oiid'], $_POST['qty'])]);
    }

    public function actionDeleteOrderItem($oid, $oiid)
    {
        $order = $this->findModel($oid);
        $order->deleteItem($oiid);
        return $this->redirect(["{$this->rootController}/update", 'id' => $oid, '#' => 'items']);
    }
}
