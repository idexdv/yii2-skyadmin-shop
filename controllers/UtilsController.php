<?php

namespace skmnt\skyadmin\shop\controllers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\Controller;
use common\models\Product;
use common\models\Dictionary;

class UtilsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionDictionaryTypedFeed($type, $q) {
        $out = ['results' => [['id' => '', 'text' => '']]];
        if ($q !== null) {
            $data = Dictionary::find()
                        ->select('id, title as text')
                        ->where(['like', 'title', $q])
                        ->andWhere(['type' => $type])
                        ->limit(15)
                        ->asArray()->all();

            $out['results'] = array_values($data);
        }
        return $this->asJson($out);
    }

    public function actionProductFamilyFeed($q)
    {
        $out = ['results' => [['id' => '', 'text' => '']]];
        if ($q !== null) {
            $data = product::find()
                        ->select('id, title as text')
                        ->where(['or', ['like', 'title', $q], ['id' => $q]])
                        ->limit(15)
                        ->asArray()->all();

            $out['results'] = array_values($data);
        }
        return $this->asJson($out);
    }

    public function actionDictionaryClean()
    {
        foreach (Dictionary::find()->where(['not', ['type' => 1]])->each() as $item) {
            $item->delete();
        }
        \Yii::$app->session->setFlash('success', 'Очистка справочников выполнена успешно.');
        return $this->redirect(['/admin']);
    }

    public function actionProductClean()
    {
        foreach (Product::find()->each() as $item) {
            $item->delete();
        }
        \Yii::$app->session->setFlash('success', 'Очистка каталога товаров выполнена успешно.');
        return $this->redirect(['/admin']);
    }
}

