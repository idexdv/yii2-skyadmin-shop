<?php

namespace skmnt\skyadmin\shop\controllers;

use Yii;
use yii\web\Controller;
use common\models\Dictionary;
use skmnt\skyadmin\shop\models\DictionarySearch;

class DictionaryController extends Controller
{
    use \admin\components\ControllerTrait;
    use \admin\components\ControllerCUDTrait;
    use \admin\components\ControllerBulkActionTrait;

    private $type;
    private $baseClass = Dictionary::class;
    private $searchClass = DictionarySearch::class;
    private $rootController = '/skyadmin-shop/dictionary';

    public function actionCreate_linkpage($link_page_id)
    {
    	// return $this->redirect([''])
    }
}
