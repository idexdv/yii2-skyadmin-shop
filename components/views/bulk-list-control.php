<?
use yii\helpers\Html;

$this->registerJs("
    function saveIdList2Cookie(addChecked = true){
        var idList = $('.grid-view td.checkbox-column input[type=\"checkbox\"]:checked').map(function(){ return this.value; }).get().join(),
            taIdlist = $('#group-edit-form textarea[name=\"ids\"]'),
            newIdlist;
        if(addChecked) newIdlist = $(taIdlist).val().length > 0 ? $(taIdlist).val() + ',' + idList : idList; else newIdlist = $(taIdlist).val();
        $(taIdlist).val(newIdlist);
        if(newIdlist.length > 0) $('#group-edit').collapse('show');
        Cookies.set('group-id-list', newIdlist, { expires: 1, path: '/' });
    }
    $('#add-checked-2list').click(function(){ saveIdList2Cookie(); });
    $('#group-edit-form textarea[name=\"ids\"]').change(function(){ saveIdList2Cookie(false) });
    $('#group-edit-form textarea[name=\"ids\"]').val(Cookies.get('group-id-list'));
    if($('#group-edit-form textarea[name=\"ids\"]').val().length > 0) $('#group-edit').collapse('show');
    $('.grid-view .checkbox-column input[type=\"checkbox\"]').change(function(){
        var cnt = $('.grid-view td.checkbox-column input[type=\"checkbox\"]:checked').length;
        if(cnt > 0) {
            $('.bottom-stack .check-count').html('('+cnt+')');
            $('.bottom-stack').fadeIn();
        }
        else
            $('.bottom-stack').fadeOut();
    });

    $('#group-edit-form button[data-action]').click(function(){
        var form = $(this).closest('form');
        $(form).append('<input type=\"hidden\" name=\"action\" value=\"' + $(this).data('action') + '\"/>');
        $(form).submit();
    });
");
?>

<div class="bottom-stack" style="display: none; background-color: rgba(154, 202, 155, 0.89);">
    <?= Html::button('<i class="fa fa-floppy-o"></i> Внести выбранное в список груп.обработки <span class="check-count"></span>', ['class' => 'btn btn-sm btn-primary', 'id' => 'add-checked-2list']) ?>
</div>