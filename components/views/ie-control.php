

<div class="btn-group">
<button id="w8" class="btn dropdown-toggle" style="margin-top: 15px; margin-bottom; 15px;" data-toggle="dropdown">Работа с данными <span class="caret"></span></button>
<ul id="w9" class="dropdown-menu"><li><a href="#" tabindex="-1" data-toggle="modal" data-target="#upload-modal"><i class="fa fa-upload text-success"></i> Загрузить в БД</a></li>
<li><a href="/skyadmin-shop/product/index?download=1" tabindex="-1"><i class="fa fa-download text-danger"></i> Выгрузить (<?= count($dataProvider->models) ?> шт.)</a></li></ul>
</div>

<?
    \yii\bootstrap\Modal::begin(['id' => 'upload-modal', 'header' => '<h3>Загрузить файл</h3>']);
    $form = \yii\widgets\ActiveForm::begin(['action' => ['import-data'], 'options' => ['enctype' => 'multipart/form-data']]);
    echo $form->field($model = new \skmnt\skyadmin\shop\models\DataUploadForm, 'dataFile')->fileInput()->label(false);
    echo $form->field($model, 'createIfNew')->checkbox();
    echo $form->field($model, 'config');
    echo \yii\helpers\Html::submitButton('Загрузить', ['class' => 'btn btn-primary']);
    \yii\widgets\ActiveForm::end();
    \yii\bootstrap\Modal::end();
?>
