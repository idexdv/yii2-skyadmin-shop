<?php
namespace skmnt\skyadmin\shop\components;

trait DataExportTrait
{
    private static $ieHelper = 'skmnt\skyadmin\shop\helpers\%sIE';
    private static $ieVendorHelper = 'common\helpers\%sImport';
    private $_config;

    private function downloadData($dataProvider, $searchModel)
    {
        $dataClass = $this->baseClass;
        $ieHelperClass = sprintf(self::$ieHelper, basename($dataClass));
        $helper = new $ieHelperClass();
        $helper->exportQuery($dataProvider->query);
        $helper->getXlsFile(sprintf('%s-%s.xlsx', basename($dataClass), date('dmy-Hi')));
    }

    public function actionImportData()
    {
    	if (\Yii::$app->request->isPost) {
            set_time_limit(6000);
        	$this->uploadData();
        }
        return $this->redirect(\Yii::$app->request->referrer ?: ['/admin']);
    }

    private function uploadData()
    {
    	$uploadFile = new \skmnt\skyadmin\shop\models\DataUploadForm;
    	$uploadFile->load(\Yii::$app->request->post());
    	$uploadFile->dataFile = \yii\web\UploadedFile::getInstance($uploadFile, 'dataFile');
    	$uploadFile->upload();
    	if ($uploadFile->validate()) {

            $this->parseConfig($uploadFile->config);

	        $dataClass = $this->baseClass;

            if (isset($this->_config['vendor'])) {
                $__baseName = \yii\helpers\Inflector::camel2words($this->_config['vendor']);
                $ieHelperClass = sprintf(self::$ieVendorHelper, $__baseName);
            } else {
                $__baseName = explode('\\', $dataClass);
	            $ieHelperClass = sprintf(self::$ieHelper, end($__baseName));
           }

	        $helper = new $ieHelperClass();
	        $helper->importFile($uploadFile->filePath, $uploadFile->createIfNew, @$this->_config['forceCreate'], explode(';', @$this->_config['page']));
	        \Yii::$app->session->setFlash('success', 'Файл импорта успешно прочитан, обработано - ' . $helper->totalRead . ' записей.');
	    } else {
        	\Yii::$app->session->setFlash('danger', 'Файл импорта содержит ошибки, либо не может быть прочитан.');
	    }
    }

    private function parseConfig($configInput)
    {
        $_pairs = explode(',', $configInput) ?: [];
        foreach ($_pairs as $pair) {
            $pair = str_replace(' ', '', $pair);
            @list($key, $value) = explode(':', $pair);
            if ($key && $value) {
                $this->_config[$key] = $value;
            }
        }
    }
}
