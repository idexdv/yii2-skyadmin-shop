<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="bottom-stack" style="display: none;" id="bottomToast">
    <?= Html::button('<i class="fa fa-floppy-o"></i> Добавить в список <span class="check-count"></span>', ['class' => 'btn btn-sm btn-primary', 'id' => 'add-checked-2list']) ?>
    <?= Html::a('Список <i class="fa fa-arrow-down"></i>', '#bulkIdList', ['class' => 'btn btn-sm btn-warning']) ?>
    <?= Html::button('Закрыть', ['class' => 'btn btn-sm btn-success', 'onClick' => "$('#bottomToast').toggle();"]) ?>
</div>


<div id="bulkIdList" class="collapse">
    <div class="well">
        <h4>Групповая обработка <small>(список ID через запятую)</small></h4>
        <div class="row">
            <? ActiveForm::begin(['id' => 'group-edit-form', 'action' => ['bulk-edit']]); ?>
                <div class="form-group col-sm-12">
                    <textarea name="ids" class="form-control" rows="4" style="resize: none;" required></textarea>
                </div>
                <div class="col-sm-12">
                    <?= Html::a('Очистить список', null, ['class' => 'btn btn-default pull-left', 'onClick' => "$('#group-edit-form textarea').val(''); $('.bottom-stack').fadeOut(); $('#bulkIdList').collapse('hide'); Cookies.set('group-id-list', '', { expires: 1, path: '/' });", 'style' => 'cursor: pointer;']) ?>
                    <?= Html::submitButton('<i class="glyphicon glyphicon-floppy-saved"></i> Обработать', ['class' => 'btn btn-warning pull-right']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<?
$this->registerJs("
    function saveIdList2Cookie(addChecked = true){
        var idList = $('.{$bulkFormClass} td.checkbox-column input[type=\"checkbox\"]:checked').map(function(){ return this.value; }).get().join(),
            taIdlist = $('#group-edit-form textarea[name=\"ids\"]'),
            newIdlist;
        if(addChecked) newIdlist = $(taIdlist).val().length > 0 ? $(taIdlist).val() + ',' + idList : idList; else newIdlist = $(taIdlist).val();
        $(taIdlist).val(newIdlist);
        if(newIdlist.length > 0) $('#bulkIdList').collapse('show');
        Cookies.set('group-id-list', newIdlist, { expires: 1, path: '/' });
    }
    $('#add-checked-2list').click(function(){
        saveIdList2Cookie();
    });
    $('#group-edit-form textarea[name=\"ids\"]').change(function(){ saveIdList2Cookie(false) });
    $('#group-edit-form textarea[name=\"ids\"]').val(Cookies.get('group-id-list'));
    if($('#group-edit-form textarea[name=\"ids\"]').val().length > 0) {
        $('#bulkIdList').collapse('show');
        $('#bottomToast').fadeIn();
    }

    $('.{$bulkFormClass} .checkbox-column input[type=\"checkbox\"]').change(function(){
        var cnt = $('.{$bulkFormClass} td.checkbox-column input[type=\"checkbox\"]:checked').length;
        if(cnt > 0) {
            $('.bottom-stack .check-count').html('('+cnt+')');
            $('.bottom-stack').fadeIn();
        }
        else
            $('.bottom-stack').fadeOut();
    });

    $('#group-edit-form button[data-action]').click(function(){
        var form = $(this).closest('form');
        $(form).append('<input type=\"hidden\" name=\"action\" value=\"' + $(this).data('action') + '\"/>');
        $(form).submit();
    });
");

?>
