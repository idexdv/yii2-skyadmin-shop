<?php

/*
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 */

namespace skmnt\skyadmin\shop\helpers;

use common\models\Product;

class ProductIE extends BaseImportExport
{
    public $columns = [
        ['id'],
        ['family_id'],
        ['code', 'format' => 'lowcase'],
        ['title'],
        ['price', 'format' => 'decimal'],
        ['price_old', 'format' => 'decimal'],
        ['is_hit', 'format' => 'yn'],
        ['is_new', 'format' => 'yn'],
        ['active', 'format' => 'yn'],
        ['description'],
        ['content', 'format' => 'text-formatted'],
    ];

    public $customImport = [
        'тов группа' => ['attribute' => 'category@prop'],
        'бренд' => ['attribute' => 'brand@prop'],
        'название' => ['attribute' => 'title@parseTitle'],
        'коллекция' => ['attribute' => 'collection@prop'],
        'страна' => ['attribute' => 'country@prop'],
        'фото' => ['attribute' => '_photofile'],
        'артикул' => ['attribute' => 'code'],
        'цвет' => ['attribute' => 'color@prop'],
        'класс' => ['attribute' => 'protection@prop'],
        'размер' => ['attribute' => 'size@parseSize'],
        'фаска' => ['attribute' => 'chamfer@prop'],
        'соединение' => ['attribute' => 'connection@prop'],
        'цена' => ['attribute' => 'price', 'format' => 'decimal'],
    ];

    public $columnIds = [];
    private $columnRules = [];
    private $extraPropCodes = [];

    public function __construct($config = [])
    {
        $this->columnIds = array_column($this->columns, 0);
        $this->columnRules = [];
        foreach ($this->columns as $key => $value) {
            if (isset($value['format'])) {
                $this->columnRules[$value[0]] = $value['format'];
            }
        }

        $this->extraPropCodes = [];
        foreach ((new Product)->extraFormFields as $value) {
            if (isset($value['items'])) {
                @list($_place, $dictionaryTypeName) = explode('::', $value['items']);
                if ($dictionaryTypeName) {
                    $this->extraPropCodes[$value['name']] = $dictionaryTypeName;
                }
            }
        }

        parent::__construct($config);
        $this->sheetsToRead = [
            // 'винил',
            // 'кварц.винил',
            // 'ламинат',
            // 'пробковые полы',
            'паркетная доска',
            'массивная доска',
            
        ];
    }

    public function fetchQuery($query)
    {
        $items= [];
        foreach ($query->each() as $model) {
            $item = array_replace(array_flip($this->columnIds),
                array_intersect_key($model->attributes, array_flip($this->columnIds)));
            $items[] = $this->makeExportable($item);
        }
        return $items;
    }

    public function makeExportable(Array $item)
    {
        foreach ($item as $key => &$column) {
            $rule = @$this->columnRules[$key];
            if ($rule !== null) {
                $column = $this->format($rule, $column);
            } elseif (empty($column)) {
                $column = '';
            }
        }
        return $item;
    }

    /**
     * Format values to export to xlsx file
     */
    public function format($ruleName, $value, $backFormat = false)
    {
        switch ($ruleName) {
            case 'yn':
                if ($backFormat) {
                    $value = strtolower($value);
                    switch ($value) {
                        case 'да':
                        case 'д':
                        case 'y':
                        case '+':
                        case '1':
                            $value = 1;
                            break;
                        default:
                            $value = 0;
                            break;
                    }
                } else {
                    if ($value) { $value = 'да'; } else { $value = 'нет'; }
                }
                break;
            case 'text-formatted':
                if ($backFormat) {
                    $value = \yii\helpers\Html::decode($value);
                } else {
                    $value = \yii\helpers\Html::encode($value);
                }
                break;
            case 'decimal':
                if ($backFormat) {
                    $value = str_replace(',', '.', $value);
                    $value = preg_replace('|[^0-9.]|', '', $value);
                }
                $value = round((float) $value, 2);
                break;
            case 'lowcase':
                $value = strtolower($value);
                break;
        }
        return $value;
    }

    public function importItem(Array $item, $createIfNew)
    {
        /**
         * Format data from import source
         */
        $this->makeImportable($item);

        if (isset($item['id'])) {
            $model = \common\models\Product::findOne(['id' => $item['id']]);
        } elseif (isset($item['code'])) {
            $model = \common\models\Product::findOne(['code' => $item['code']]);
        }

        /**
         * If model doesn't exist create new if such option selected by user.
         */
        if (!@$model) {
            if ($createIfNew) {
                $model = new \common\models\Product(['active' => 1]);
            } else {
                return null;
            }
        } else {
        	/**
        	 * Save json-fields not existing in import source.
        	 */
        	$item['prop'] = array_replace($model->prop, $item['prop']);
        }

        if (!$model->load(['Product' => $item]) || !$model->save()) return false;

        /**
         * Check for attached image files, and import them.
         * By the "code" fieled.
         */
        if (@$item['_photofile'] && $model->getImages()->count() == 0) {
            $imageFiles = array_merge(
                glob(sprintf(\Yii::$app->params['localUploadPath'] . "/%s.jpg", strtolower($item['_photofile']))),
                glob(sprintf(\Yii::$app->params['localUploadPath'] . "/%s_*.jpg", strtolower($item['_photofile'])))
            );
            foreach ($imageFiles as $filename) {
                if (!$model->importImage($filename, false)) {
                    throw new \yii\web\ServerErrorHttpException(sprintf('Ошибка при импорте файла изображения %s для записи #%d', $filename, $model['id']));
                }
            }
        }

        return true;
    }

    private function makeImportable(&$item)
    {
    	$keys = array_map('trim', array_keys($item));
    	$item = array_map('trim', array_values($item));
    	$item = array_combine($keys, $item);
        $oldItem = $item;

        $_prop = [];
        $_extra = [];
        foreach ($item as $key => &$column) {
            $_key = trim($key);
            $column = trim($column);

            if (($rule = @$this->columnRules[$_key]) !== null) {
                $column = $this->format($rule, $column, true);
            } elseif (($rule = @$this->customImport[$_key]) !== null) {
                @list($attrName, $scopeName) = explode('@', $rule['attribute']);
                switch ($scopeName) {
                    case 'prop':
                        $_prop[$attrName] = \common\models\Dictionary::getOrSet(@$this->extraPropCodes[$attrName], $column);
                        break;
                    case 'parseSize':
                        if ($attrName == 'size') {
                            @list($l,$w,$t) = explode('x', $column);
                            $_prop['length'] = $l;
                            $_prop['width'] = $w;
                            $_prop['thickness'] = $t;
                        }
                        break;
                    case 'parseTitle':
                    	if (empty($column)) {
                    		$parts = [$oldItem['артикул'], $oldItem['цвет'], $oldItem['тов группа'], $oldItem['бренд']];
                    		$parts = array_filter($parts);
	                    	$column = implode(', ', $parts);
                    	}
                    	$item[$attrName] = $column;
                    	break;
                    default:
                        if (($rule = @$this->columnRules[$attrName]) !== null) {
                            $column = $this->format($rule, $column, true);
                        }
                        $_extra[$attrName] = $column;
                        break;
                }
                unset($item[$key]);
            }
        }
    	$item = array_filter($item);
        $item = array_replace($item, $_extra);
        $item['prop'] = $_prop;
    }
}
