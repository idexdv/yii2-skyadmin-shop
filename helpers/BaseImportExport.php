<?php

/*
 * (c) Dmitry Vikharev <idexdv@gmail.com>
 */

namespace skmnt\skyadmin\shop\helpers;

use common\models\Product;
use common\models\Dictionary;

class BaseImportExport extends \yii\base\BaseObject
{
    public $items = [];
    public $totalRead;
    public $sheetsToRead;

    public function __construct($config = [])
    {
        $this->columnIds = array_column($this->columns, 0);
        $this->columnRules = [];
        foreach ($this->columns as $key => $value) {
            if (isset($value['format'])) {
                $this->columnRules[$value[0]] = $value['format'];
            }
        }

        $this->extraPropCodes = [];
        foreach ((new Product)->extraFormFields as $value) {
            if (isset($value['items'])) {
                @list($_place, $dictionaryTypeName) = explode('::', $value['items']);
                if ($dictionaryTypeName) {
                    $this->extraPropCodes[$value['name']] = $dictionaryTypeName;
                }
            }
        }
        parent::__construct($config);
    }

    public function fetchQuery($query)
    {
        $items= [];
        foreach ($query->each() as $model) {
            $item = array_replace(array_flip($this->columnIds),
                array_intersect_key($model->attributes, array_flip($this->columnIds)));
            $items[] = $this->makeExportable($item);
        }
        return $items;
    }

    public function makeExportable(Array $item)
    {
        foreach ($item as $key => &$column) {
            $rule = @$this->columnRules[$key];
            if ($rule !== null) {
                $column = $this->format($rule, $column);
            } elseif (empty($column)) {
                $column = '';
            }
        }
        return $item;
    }

    public function exportQuery(\yii\db\Query $query)
    {
        $this->items = $this->fetchQuery($query);
    }

    public function getXlsFile($fileName)
    {
        if (!is_array($this->items)) return;

        $columns = array_map(function($el){
            return [
                'attribute' => $el,
                'header' => $el,
                'format' => 'text',
            ];
        }, $this->columnIds);
        \moonland\phpexcel\Excel::widget([
            'models' => $this->items,
            'mode' => 'export',
            'columns' => $columns,
            'fileName' => $fileName,
        ]);
    }

    public function importFile($filePath, $createIfNew = false, $forceCreate = false, $pageToImport = [])
    {
        if (!empty($pageToImport)) $this->sheetsToRead = $pageToImport;

        $this->totalRead = 0;
        if (is_array($this->sheetsToRead)) {
            foreach ($this->sheetsToRead as $sheetName) {
                $this->importDataTable(\moonland\phpexcel\Excel::widget([
                    'mode' => 'import', 
                    'fileName' => $filePath, 
                    'setFirstRecordAsKeys' => true,
                    'setIndexSheetByName' => true,
                    'getOnlySheet' => $sheetName,
                ]), $createIfNew, $forceCreate);
            }
        } else {
            $this->importDataTable(\moonland\phpexcel\Excel::widget([
                'mode' => 'import', 
                'fileName' => $filePath, 
                'setFirstRecordAsKeys' => true,
                'setIndexSheetByName' => true,
            ]), $createIfNew, $forceCreate);
        }
    }

    private function importDataTable($data, $createIfNew, $forceCreate)
    {
        if (!is_array($data) || empty($data)) {
            \Yii::$app->session->setFlash('danger', 'Файл импорта не содержит записей, либо не найдены.');
            return false;
        }

        foreach ($data as $key => $item) {
            if ($this->importItem($item, $createIfNew, $forceCreate)) {
                $this->totalRead++;
            }
        }
    }

    /**
     * Format values to export to xlsx file
     */
    public function format($ruleName, $value, $backFormat = false)
    {
        switch ($ruleName) {
            case 'yn':
                if ($backFormat) {
                    $value = strtolower($value);
                    switch ($value) {
                        case 'да':
                        case 'д':
                        case 'y':
                        case '+':
                        case '1':
                            $value = 1;
                            break;
                        default:
                            $value = 0;
                            break;
                    }
                } else {
                    if ($value) { $value = 'да'; } else { $value = 'нет'; }
                }
                break;
            case 'text-formatted':
                if ($backFormat) {
                    $value = \yii\helpers\Html::decode($value);
                } else {
                    $value = \yii\helpers\Html::encode($value);
                }
                break;
            case 'decimal':
                if ($backFormat) {
                    $value = str_replace(',', '.', $value);
                    $value = preg_replace('|[^0-9.]|', '', $value);
                }
                $value = round((float) $value, 2);
                break;
            case 'lowcase':
                $value = strtolower($value);
                break;
        }
        return $value;
    }

    public function importItem(Array $item, $createIfNew)
    {
        /**
         * Format data from import source
         */
        if ($this->makeImportable($item) === false) return;

        if (isset($item['id'])) {
            $model = \common\models\Product::findOne(['id' => $item['id']]);
        } elseif (isset($item['code'])) {
            $model = \common\models\Product::find()
                            ->select(['*', 'JSON_UNQUOTE(json_extract(prop, "$.brand")) as p_brand'])
                            ->where(['code' => $item['code']])
                            ->having(['p_brand' => @$item['prop']['brand']])
                            ->one();
        }

        /**
         * If model doesn't exist create new if such option selected by user.
         */
        if (!@$model) {
            if ($createIfNew) {
                $model = new \common\models\Product(['active' => 1]);
            } else {
                return null;
            }
        } else {
            /**
             * Save json-fields not existing in import source.
             */
            $item['prop'] = array_replace($model->prop, $item['prop']);
        }

        if (!$model->load(['Product' => $item]) || !$model->save()) {
            print_r($model);die;
            return false;
        }

        if (!empty($item['_photofile']) && $model->getImages()->count() == 0) {
            $photos = is_array($item['_photofile']) ? $item['_photofile'] : [$item['_photofile']];
            foreach ($photos as $_photofile) {

                @list($src, $file) = explode(':', $_photofile);
                switch ($src) {
                    case 'local':
                        $file = \Yii::getAlias('@localUploads' . $file);
                        $model->importImage($file, false /* False is to Don't delete source file */ );
                        break;
                    case 'http':
                        $model->importImageByURL($_photofile);
                        break;
                }
            }
        }
        return true;
    }
}
