<?php
namespace skmnt\skyadmin\shop\helpers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\models\Dictionary;

/**
* 
*/
class ProductBulkForm extends \yii\base\Widget
{
    public $bulkFormClass;

    public function run()
    {
        $html = [];

        $html[] = $this->renderIdListControl();
        return implode($html);
    }

    private function renderIdListControl()
    {
        return $this->render('bulk-id-list', ['bulkFormClass' => $this->bulkFormClass]);
    }
}
