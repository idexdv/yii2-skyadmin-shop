<?php
namespace skmnt\skyadmin\shop\helpers;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use common\models\Dictionary;

/**
* 
*/
class ProductFormProperties extends \yii\base\Widget
{
    public $form;
    public $model;

    public function run()
    {
        $tagSelectors = [];
        foreach (Dictionary::$controllerIds as $type => $name) {
            $tagSelectors[] = $this->renderTagSelector($type, $name);
        }

        echo '<h4 class="col-sm-12">Свойства товара</h4>';
        echo implode($tagSelectors);
    }

    private function renderTagSelector($type, $name)
    {
        return
            $this->form->field($this->model, "_tags[{$type}]", ['options' => ['class' => 'no-form-group col-md-6', 'style' => 'min-height: 69px;']])->widget(\kartik\select2\Select2::classname(), [
                'options' => ['placeholder' => 'Поиск свойств ...', 'multiple' => true],
                'data' => \yii\helpers\ArrayHelper::map($this->model->getDictionaries()->select('title as text, id')->asArray()->all(), 'id', 'text'),
                'maintainOrder' => true,
                'pluginOptions' => [
                    'allowClear' => true,
                    // 'minimumInputLength' => 2,
                    'maximumInputLength' => 10,
                    'language' => 'ru',
                    'ajax' => [
                        'url' => Url::to(['utils/dictionary-typed-feed', 'type' => $type]),
                        'dataType' => 'json',
                        'delay' => 200,
                        'cache' => true,
                        'data' => new JsExpression('function(params) { return {q: params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(item) { return item.text; }'),
                    'templateSelection' => new JsExpression('function (item) { return item.text; }'),
                ]
            ])->label(Dictionary::$controllerTitles[$type]);
    }
}
