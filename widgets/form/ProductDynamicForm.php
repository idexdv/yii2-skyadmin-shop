<?php

namespace skmnt\skyadmin\shop\widgets\form;

use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Dictionary;

/**
* Product Dynamic Form widget
*/
class ProductDynamicForm extends \admin\widgets\form\DynamicForm
{
    public function run()
    {
        if (!($category = Dictionary::findOne($this->model->prop['category']))) return;
        if (!($categoryBrandedConfig = @$category->prop_cfg['brand_' . @$this->model->prop['brand']])) return;

        $catProperites = array_keys($categoryBrandedConfig);

        foreach ($categoryBrandedConfig as $name => $item) {
            $item['fieldOptions']['class'] = @$item['wrapClass'] ?: 'col-sm-3';
            $item['label'] = (@$item['label'] ?: \yii\helpers\Inflector::humanize("{$this->extraFieldPrefix}"));
            if (@$item['ei']) $item['label'] .= (', ' . $item['ei']);
            $item['name'] = "{$this->extraFieldPrefix}[{$name}]";
            if ($renderMethod = 'fieldRender' . \yii\helpers\Inflector::camelize($item['type'])) {
                $this->$renderMethod($item);
            }
        }

        if ($this->enableCkEditor) $this->registerCkEditor();
        $this->registerJs();
    }
}