<?php

namespace skmnt\skyadmin\shop;

use common\models\Dictionary;

/**
 * Admin shop module definition class
 */
class Module extends \skmnt\skyadmin\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = null;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // \Yii::setAlias('@uploadDir', $uploadDir = \Yii::getAlias('@runtime') . '/temp_upload');
        // if (!file_exists($uploadDir)) {
        //     @mkdir($uploadDir, 0777, true);
        // } 
    }
}
