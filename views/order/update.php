<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */

$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
$this->title = $model->order_code . ' (изменение)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="custom-dictionary-update">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
            [
                'label' => 'Товары',
                'content' => $this->render('_orderItems', ['model' => $model]),
                'options' => ['id' => 'items'],
            ],
        ],
        'clientEvents' => [
            'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
        ],
    ]) ?>

    <? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

</div>
