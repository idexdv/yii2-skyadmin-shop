<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="well">
    <div class="row">

        <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

            <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">' . $_errorHtml . '</div>' : '' ?>

            <? if (!@$isCreateMode): ?>

                <?= $form->field($model, 'order_code', ['options' => ['class' => 'col-md-5']])->textInput(['readonly' => true, 'disabled' => true, 'tabindex' => -1]) ?>

                <div class="col-sm-4">
                    <label class="control-label">Ссылка на статус заказа</label>
                    <input style="cursor: text;" type="text" class="form-control" readonly disabled tabindex="-1" value="<?= Url::to(['/site/zakaz', 'code' => strtolower($model->order_code)], true) ?>">
                </div>

                <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-3']])->dropDownList(['1' => 'Активен', '0' => 'Выключен']) ?>
                <div class="clearfix"></div>
            <? endif; ?>

            <?= \admin\widgets\form\DynamicForm::widget(['form' => $form, 'model' => $model]) ?>

            <div class="clearfix"></div>
            <div class="form-group col-md-12">
                <?= Html::a('Сохранить и закрыть', null, [
                    'onclick' => 'SaveAndExit(this);',
                    'class' => 'pull-right btn btn-success',
                ]) ?>
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
