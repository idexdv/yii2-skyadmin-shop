<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use admin\widgets\GridView2;
use kartik\select2\Select2;
use common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\DictionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView2::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'maxButtonCount' => 5,
            'prevPageLabel' => '<span class="glyphicon glyphicon-backward"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-forward"></span>',
            'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
            'options' => ['class' => 'pagination center'],
        ],
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'headerOptions' => ['class' => 'checkbox-column']],
            ['class' => 'admin\widgets\ControlColumn', 'template' => '{active}', 'headerOptions' => ['class' => 'control-column']],
            [
                'attribute' => 'order_code', 'format' => 'raw',
                'value' => function($model, $key) { return Html::a($model->order_code, ['/skyadmin-shop/order/update', 'id' => $model->id], ['class' => 'btn btn-sm btn-fw btn-title-link']); },
                'contentOptions' => ['style' => 'max-width: 200px; min-width: 150px;'],
            ],
            ['attribute' => 'status', 'value' => function($order) { return $order->statusTitle; }, 'filter' => Order::statusList()],
            'customer_name',
            'customer_email:email',
            'customer_phone',
            'total:currency',
            ['attribute' => 'created_at', 'value' => function($order) { return \Yii::$app->formatter->asDate($order->created_at); }, 'filter' => false,],
            ['class' => 'admin\widgets\ControlColumn', 'headerOptions' => ['class' => 'delete-column'], 'template' => '{delete}'],
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered td-v-m']
    ]); ?>
</div>
