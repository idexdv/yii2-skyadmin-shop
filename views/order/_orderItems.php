<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="well">
    <?= \yii\grid\GridView::widget([
        'id' => 'orderItems',
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getOrderItems()]),
        'columns' => [
            'id',
            'product_name',
            [
                'attribute' => 'product_count',
                'headerOptions' => ['class' => 'text-right'],
                'contentOptions' => ['data-editable' => 'item_count', 'class' => 'text-right'],
                'format' => 'raw',
                'value' => function($model) { return sprintf('<span contenteditable="true" data-old="%s">%s</span>', $model->product_count, $model->product_count); },
            ],
            'product_price',
            'product_total',
            [
                'format' => 'raw',
                'label' => '',
                'value' => function($model) {
                    return Html::a('Удалить', ['delete-order-item', 'oid' => $model->order_id, 'oiid' => $model->id], ['data-confirm' => 'Удалить товар из заказа?']);
                }
            ],
        ],
        'tableOptions' => [
            'class' => 'table table-bordered',
            'style' => 'margin-bottom: 0',
        ],
        'emptyText' => 'У этого заказа нет товаров!',
    ]) ?>
</div>

<?php $form = ActiveForm::begin(['action' => ['add-new-item', 'oid' => $model->id]]); ?>
    <div class="well">
        <h4 style="margin: 0 0 15px;">Добавить товар</h4>
        <div class="row">
            <?= Html::tag('div',
                \kartik\typeahead\Typeahead::widget([
                    'name' => 'order_item',
                    'options' => ['placeholder' => 'Начните вводить по буквам ...'],
                    'pluginOptions' => ['highlight'=>true],
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'value', 'limit' => 20,
                            'remote' => ['url' => Url::to(['product-feed']) . '?q=%QUERY', 'wildcard' => '%QUERY']
                        ]
                    ],
                    'pluginEvents' => [
                        "typeahead:select" => "function(e, suggestion) {
                            $('[name=product_id]').val(suggestion.id);
                        }",
                    ],
                ]),
                ['class' => 'col-sm-8']
            ); ?>
            <div class="form-group col-sm-4">
                <div class="input-group">
                    <input type="number" class="text-right form-control" name="product_qty" min="1" max="999999" placeholder="1">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit" data-add-new-item>Добавить</button>
                    </span>
                </div>
            </div>
            <input type="hidden" name="product_id">
        </div>
    </div>
<?php ActiveForm::end(); ?>

<style>
    #items .grid-view td[data-editable] span {
        display: block;
        padding: 2px 4px;
        min-width: 70px;
        text-align: right;
        border: 1px dotted transparent;
        outline: none !important;
    }
    #items .grid-view td[data-editable]:hover span {
        border: 1px dotted #1575bd;
        background: #daefe6;
    }
    #items .grid-view td[data-editable] span:focus {
        color: white;
        background: #318e14 !important;
    }
    .page-reloading {
        position: fixed;
        top: 0; left: 0; right: 0; bottom: 0;
        z-index: 999998;
        background: rgba(255, 255, 255, .7);
    }
    .page-reloading .fa {
        position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);
        z-index: 999999;
    }
</style>

<?
$_url = Url::to(['update-item-count', 'oid' => $model['id']]);
$this->registerJs("
    // $(document).on('change', '[qty-control]', function(e){
    $('[data-editable] span', '#orderItems').on('keydown', function(e) {
        switch (e.keyCode) {
            case 13:
                var qty = parseFloat($(this).text());
                $(this).data('old', qty);
                $('body').append('<div class=\"page-reloading\"><i class=\"fa fa-3x fa-spinner fa-spin\"></i></div>');
                $.post('{$_url}', {oiid: $(this).closest('tr').data('key'), qty: $(this).text()})
                 .done(function(data) {
                    window.location.reload();
                 });
                return false;
            case 27:
                $(this).text($(this).data('old'));
                break;
            case 190:
                return !($(this).text().match('[.]') != null);
                break;
            default:
                if (e.keyCode > 57) return false;
        }
    });
") ?>
