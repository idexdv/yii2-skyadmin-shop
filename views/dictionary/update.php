<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */

$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
foreach ($model->bloodLine as $item) {
    $this->params['breadcrumbs'][] = [
        'label' => \yii\helpers\StringHelper::truncate($item['title'],35),
        'url' => $indexAction + [$indexSearchId => ['parent_id' => $item['id']]],
    ];
}

$this->title = \yii\helpers\StringHelper::truncate($model->title, 40) . ' (изменение)';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = ['label' => '(Создать запись)', 'url' => $createAction, 'encode' => false];
?>

<div class="custom-dictionary-update">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => array_merge([
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
            [
                'label' => 'Конфигурация',
                'content' => $this->render('_config', ['model' => $model]),
                'options' => ['id' => 'config'],
            ],
        ], 
            (!in_array($model->type, $model->linkablePages) ? [] :
            (@$model->prop['linked_page'] ?
                [[
                    'label' => 'Перейти к связанной странице',
                    'url' => ['/admin/page/index/brand/update', 'id' => $model->prop['linked_page']],
                ]] : [[
                    'label' => 'Создать и привязать страницу',
                    'url' => [$createAction[0] . '_linkpage'] + ['link_page_id' => $model->id],
                ]]
            )
        )),
        'clientEvents' => [
            'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
        ],
    ]) ?>

    <? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

</div>
