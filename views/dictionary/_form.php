<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Dictionary;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel with-image-aside">

    <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

        <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">'.$_errorHtml.'</div>' : '' ?>

        <div class="panel-body">
            <div class="row">

                <? if ($model->isDepth): ?>
                    <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-9']]) ?>
                    <?= $form->field($model, 'parent_id', ['options' => ['class' => 'col-md-3']])->dropDownList($model->parentsList(), ['prompt' => 'Корневой уровень']) ?>
                <? else: ?>
                    <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-12']]) ?>
                <? endif; ?>

                <?= $form->field($model, 'slug', ['options' => ['class' => 'col-md-9']]) ?>

                <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-3']])->dropDownList(['1' => 'Активен', '0' => 'Выключен']) ?>

                <div class="clearfix"></div>

                <?= \admin\widgets\form\DynamicForm::widget(['form' => $form, 'model' => $model]) ?>

            </div>
        </div>

        <div class="panel-footer col-md-12" style="background-color: #222d32;">
            <?= Html::a('Сохранить и закрыть', null, [
                'onclick' => 'SaveAndExit(this);',
                'class' => 'pull-right btn btn-success',
            ]) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="clearfix"></div>

</div>
