<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Dictionary;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */
/* @var $form yii\widgets\ActiveForm */

if ($model->prop_cfg) {
    $model->_s_prop_cfg = json_encode($model->prop_cfg, JSON_UNESCAPED_UNICODE);
}

?>

<div class="panel with-image-aside">

    <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

        <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">'.$_errorHtml.'</div>' : '' ?>

        <div class="panel-body">
            <div class="row">
                <?= $form->field($model, '_s_prop_cfg', ['options' => ['class' => 'col-md-9']])->textArea(['rows' => 10])->label('Конфигурация (json). Опасно!') ?>
            </div>
        </div>

        <div class="panel-footer col-md-12" style="background-color: #222d32;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="clearfix"></div>

</div>
