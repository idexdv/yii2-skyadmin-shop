<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use admin\widgets\GridView2;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\DictionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if ($parent) {
    $title .= ' - ' . $parent->title;
}

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="dictionary-index">

    <p>
        <?= $parent ? Html::a('<i class="fa fa-chevron-left"></i>', ['index', 'type' => $cid, $searchModel->formName() => ['parent_id' => $parent['parent_id']]], ['class' => 'btn btn-default']) : '' ?>
        <?= Html::a('Создать', ['create', 'type' => $cid, 'parent_id' => $searchModel['parent_id']], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView2::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'maxButtonCount' => 5,
            'prevPageLabel' => '<span class="glyphicon glyphicon-backward"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-forward"></span>',
            'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
            'options' => ['class' => 'pagination center'],
        ],
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'headerOptions' => ['class' => 'checkbox-column']],
            ['class' => 'admin\widgets\ControlColumn', 'template' => '{active}', 'headerOptions' => ['class' => 'control-column']],
            [
                'class' => 'admin\widgets\ParentControlColumn',
                'modelName' => $searchModel->formName(),
                'attribute' => 'parent_id',
                'filter' => false,
                'headerOptions' => ['class' => 'control-column'],
                'visible' => $searchModel->isDepth,
            ],
            [
                'attribute' => 'title', 'format' => 'raw',
                'value' => function($model, $key) { return Html::a(mb_substr($model->title, 0, 60), ['/skyadmin-shop/dictionary/update', 'id' => $model->id], ['class' => 'btn btn-sm btn-fw btn-title-link', 'title' => Html::encode($model->title)]); },
                'headerOptions' => ['class' => 'title-column'],
                'contentOptions' => ['class' => 'title-column'],
            ],
            [
                'attribute' => 'slug', 'format' => 'raw',
                'contentOptions' => ['style' => 'max-width: 200px; min-width: 150px;'],
            ],
            [
                'attribute' => 'content', 'format' => 'raw',
                'value' => function($model, $key) { return mb_substr(strip_tags($model->content), 0, 100); },
                'contentOptions' => ['style' => 'width: auto;', 'class' => 'visible-lg'],
                'headerOptions' => ['class' => 'visible-lg'],
            ],
            [
                'attribute' => 'updated_at',
                'filter' => false,
                'format' => ['date', 'php:d-m-Y'],
                'headerOptions' => ['class' => 'date-column'],
                'contentOptions' => ['class' => 'date-column'],
            ],
            ['class' => 'admin\widgets\ControlColumn', 'headerOptions' => ['class' => 'delete-column'], 'template' => '{delete}'],
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered td-v-m']
    ]); ?>
</div>
