<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\models\Dictionary;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel with-image-aside">

    <?php $form = ActiveForm::begin(['id' => 'update-form']); ?>

        <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">' . $_errorHtml . '</div>' : '' ?>

        <div class="panel-body">
            <div class="row">

                <?= $form->field($model, 'title', ['options' => ['class' => 'col-md-6']]) ?>

                <?= $form->field($model, 'family_id', ['options' => ['class' => 'col-md-4', 'style' => 'min-height: 69px;']])
                    ->widget(\kartik\select2\Select2::classname(), [
                        'options' => ['placeholder' => 'Поиск товара ...', 'multiple' => false],
                        'initValueText' => @\common\models\Product::findOne($model->family_id)['title'] ?: '- не найдено (ошибка) -',
                        'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'maximumInputLength' => 10,
                            'language' => 'ru',
                            'ajax' => [
                                'url' => Url::to(['utils/product-family-feed']),
                                'dataType' => 'json',
                                'delay' => 200,
                                'cache' => true,
                                'data' => new JsExpression('function(params) { return {q: params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(item) { return item.text; }'),
                            'templateSelection' => new JsExpression('function (item) { return item.text; }'),
                        ]
                    ]) ?>

                <?= $form->field($model, 'active', ['options' => ['class' => 'col-md-2']])->dropDownList(['1' => 'Активен', '0' => 'Выключен']) ?>

                <div class="clearfix"></div>

                <?= $form->field($model, 'code', ['options' => ['class' => 'col-md-6']]) ?>

                <?= \admin\widgets\form\DynamicForm::widget(['form' => $form, 'model' => $model, 'seoMode' => false]) ?>
                
                <div class="col-sm-12"><hr></div>

                <?= \skmnt\skyadmin\shop\widgets\form\ProductDynamicForm::widget(['form' => $form, 'model' => $model]) ?>

            </div>
        </div>

        <div class="panel-footer col-md-12" style="background-color: #222d32;">
            <?= Html::a('Сохранить и закрыть', null, [
                'onclick' => 'SaveAndExit(this);',
                'class' => 'pull-right btn btn-success',
            ]) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

    <div class="clearfix"></div>

</div>
