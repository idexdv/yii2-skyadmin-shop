<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Групповое изменение';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Групповое изменение';
?>

<div class="product-update">

<h2>Вы вносите изменение для группы товаров</h2>

<div class="well">
	<h4>Список ID-кодов</h4>
	<p style="word-break: break-all; max-height: 60px; overflow-y: scroll;"><?= $ids ?></p>
	<hr>
	<ol>
		<li>Задаются только те поля, что заданы.</li>
		<li>Чтобы сбросить значение - указать/выбрать "!"</li>
	</ol>
</div>

<div class="bulk-form">
	<?= $this->render('_form-bulk-edit', [
	    'model' => $model,
	    'ids' => $ids,
	    'bulkMode' => true,
	]) ?>
</div>

<style type="text/css">
	.bulk-form {
		padding: 0 15px;
	}
	.bulk-form > .row {
		padding-left: 4px;
		padding-right: 4px;
	}
</style>