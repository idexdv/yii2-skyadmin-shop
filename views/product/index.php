<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;
use admin\widgets\GridView2;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\DictionarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index">

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView2::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'maxButtonCount' => 5,
            'prevPageLabel' => '<span class="glyphicon glyphicon-backward"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-forward"></span>',
            'firstPageLabel' => '<span class="glyphicon glyphicon-fast-backward"></span>',
            'lastPageLabel' => '<span class="glyphicon glyphicon-fast-forward"></span>',
            'options' => ['class' => 'pagination center'],
        ],
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn', 'headerOptions' => ['class' => 'checkbox-column'], 'contentOptions' => ['class' => 'checkbox-column']],
            'id',
            ['class' => 'admin\widgets\ControlColumn', 'template' => '{active}', 'headerOptions' => ['class' => 'control-column']],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function($model, $key) {
                    return Html::a($model->title, ['/skyadmin-shop/product/update', 'id' => $model->id], ['class' => 'btn btn-sm btn-fw btn-title-link']);
                },
                'headerOptions' => ['class' => 'title-column'],
                'contentOptions' => ['class' => 'title-column'],
            ],
            [
                'attribute' => 'code',
                'headerOptions' => ['class' => 'code-column'],
                'contentOptions' => ['class' => 'code-column', 'style' => 'font-family: monospace; font-size: 90%; font-weight: bold;'],
            ],
            [
                'attribute' => 'p_category',
                'label' => 'Категория',
                'format' => 'raw',
                'filter' => \common\models\Dictionary::listByType(\common\models\Dictionary::T_PRODUCT_CATEGORY),
                'value' => function($model) { return $model->propValue(\common\models\Dictionary::T_PRODUCT_CATEGORY, 'category'); },
                'headerOptions' => ['class' => 'category-column'],
                'contentOptions' => ['class' => 'category-column'],
            ],
            [
                'attribute' => 'price',
                'format' => ['currency', 'RUR'],
                'headerOptions' => ['class' => 'price-column'],
                'contentOptions' => ['class' => 'price-column'],
            ],
            [
                'attribute' => 'price_old',
                'format' => ['currency', 'RUR'],
                'headerOptions' => ['class' => 'price-column'],
                'contentOptions' => ['class' => 'price-column'],
            ],
            [
                'attribute' => 'updated_at',
                'filter' => false,
                'format' => ['date', 'php:d-m-Y'],
                'headerOptions' => ['class' => 'date-column'],
                'contentOptions' => ['class' => 'date-column'],
            ],
            ['class' => 'admin\widgets\ControlColumn', 'headerOptions' => ['class' => 'delete-column'], 'template' => '{delete}'],
        ],
        'tableOptions' => ['class' => 'table table-striped table-bordered ' . ($_bulkFormClass = 'bulk-form-grid')]
    ]); ?>

    <?= (@$_GET['__ie']) ? $this->render('../../components/views/ie-control', ['dataProvider' => $dataProvider, 'target' => 'Product']) : '' ?>

    <?= \skmnt\skyadmin\shop\helpers\ProductBulkForm::widget(['bulkFormClass' => $_bulkFormClass]) ?>

</div>
