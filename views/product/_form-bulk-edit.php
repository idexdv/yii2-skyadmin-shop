<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use common\models\Dictionary;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="panel with-image-aside">

    <?php $form = ActiveForm::begin(['id' => 'update-form', 'action' => ['bulk-edit-save']]); ?>

        <?= ($_errorHtml = $form->errorSummary($model)) ? '<div class="col-sm-12 text-danger">' . $_errorHtml . '</div>' : '' ?>

        <div class="panel-body">
            <div class="row">
                <?= \admin\widgets\form\DynamicForm::widget([
                    'form' => $form,
                    'model' => $model,
                    'overrideFields' => ['except' => ['slug']],
                    'bulkMode' => $bulkMode,
                ]) ?>
            </div>
        </div>

        <div class="panel-footer col-md-12" style="background-color: #222d32;">
            <?= Html::submitButton('Сохранить для всех', ['class' => 'btn btn-primary']) ?>
        </div>

        <?= Html::hiddenInput('ids', $_POST['ids']) ?>

    <?php ActiveForm::end(); ?>

    <div class="clearfix"></div>

</div>
