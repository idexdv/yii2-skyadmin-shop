<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Dictionary */

$this->title = sprintf('%s [#%d]', $_title = \yii\helpers\StringHelper::truncate($model->title, 60), $model->id);
$this->params['breadcrumbs'][] = ['label' => $indexTitle, 'url' => $indexAction];
$this->params['breadcrumbs'][] = $_title . ' (изменение)';
$this->params['breadcrumbs'][] = ['label' => '(Создать запись)', 'url' => @$createAction, 'encode' => false];
?>
<div class="custom-product-update">

    <?= \yii\bootstrap\Tabs::widget([
        'items' => [
            [
                'label' => 'Общее',
                'content' => $this->render('_form', ['model' => $model]),
                'active' => true,
                'options' => ['id' => 'general'],
            ],
            [
                'label' => 'Фото',
                'content' => $this->render('_image', ['model' => $model]),
                'options' => ['id' => 'image'],
            ],
            [
                'label' => '<i class="fa fa-cogs" style="font-size: 12px;"></i> SEO',
                'encode' => false,
                'content' => $this->render('_seo', ['model' => $model]),
                'options' => ['id' => 'seo'],
            ],
        ],
        'clientEvents' => [
            'show.bs.tab' => new \yii\web\JsExpression('function(e) { location.hash = e.target.hash; }'),
        ],
    ]) ?>

    <? $this->registerJs("var hash = window.location.hash; hash && $('ul.nav a[href=\"' + hash + '\"]').tab('show');"); ?>

</div>
