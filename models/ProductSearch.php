<?php

namespace skmnt\skyadmin\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Product;

class ProductSearch extends Product
{
    public $search;
    public $p_category;

    public function rules()
    {
        return [
            ['search', 'trim'],
            ['search', 'string'],
            [['id', 'p_category'], 'integer'],
            [['code', 'title'], 'string'],
            [['price'], 'number'],
        ];
    }

    public function behaviors() {
        return [];
    }

    public function beforeValidate()
    {
        return \yii\base\Model::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    // public function getParent()
    // {
    //     return $this->parent_id ? Product::findOne($this->parent_id) : null;
    // }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->select(['*', 'JSON_UNQUOTE(json_extract(prop, "$.category")) as p_category']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                'created_at' => SORT_DESC,
            ]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->search)) {
            $query
                ->andWhere(['or',
                    ['like', 'slug', $this->search],
                    ['like', 'title', $this->search],
                    ['like', 'static_url', $this->search],
                ]);
        } else {
            $query
                ->andFilterWhere(['id' => $this->id])
                ->andFilterWhere(['price' => $this->price])
                ->andFilterWhere(['like', 'code', $this->code])
                ->andFilterWhere(['like', 'title', $this->title])
                ->filterHaving(['p_category' => $this->p_category]);
        }

        return $dataProvider;
    }
}
