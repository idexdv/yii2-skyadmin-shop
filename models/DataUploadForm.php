<?

namespace skmnt\skyadmin\shop\models;

use yii\base\Model;
use yii\web\UploadedFile;

class DataUploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $priceOnly;
    public $createIfNew = false;
    public $dataFile;
    public $filePath;
    public $destPath = '@runtime/temp_upload';
    public $config;

    public function rules()
    {
        return [
            [
                ['dataFile'], 'file',
                'skipOnEmpty' => false, 'extensions' => ['xls', 'xlsx'],
                'checkExtensionByMimeType' => false,
                'maxSize' => 20000000,
            ],
            [['priceOnly'], 'boolean'],
            [['createIfNew'], 'boolean'],
            ['config', 'string']
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'priceOnly' => 'Взять только цены',
            'createIfNew' => 'Создать запись, если не найдено по ID',
            'config' => 'Конфигурация импорта',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            if(!file_exists(\Yii::getAlias($this->destPath))) {
                mkdir(\Yii::getAlias($this->destPath), 0x0700);
            }
            $this
                ->dataFile
                ->saveAs($this->filePath = \Yii::getAlias($this->destPath) . '/' . hash('md5', $this->dataFile->baseName) . '.' . $this->dataFile->extension);
            return true;
        } else {
            return false;
        }
    }
}
