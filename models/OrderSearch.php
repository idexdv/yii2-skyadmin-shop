<?php

namespace skmnt\skyadmin\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

class OrderSearch extends Order
{
    public $search;

    public function rules()
    {
        return [
            ['search', 'trim'],
            ['search', 'string'],
            [['status', 'created_at'], 'integer'],
            [['order_code', 'customer_name', 'customer_phone', 'customer_email'], 'string'],
        ];
    }

    public function behaviors() {
        return [];
    }

    public function beforeValidate()
    {
        return \yii\base\Model::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    // public function getParent()
    // {
    //     return $this->parent_id ? Order::findOne($this->parent_id) : null;
    // }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => [
                'created_at' => SORT_DESC,
            ]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!empty($this->search)) {
            $query
                ->andWhere(['or',
                    ['like', 'customer_name', $this->search],
                    ['like', 'customer_phone', $this->search],
                    ['like', 'customer_email', $this->search],
                    ['like', 'customer_address', $this->search],
                ]);
        } else {
            $query
                ->andFilterWhere(['status' => $this->status])
                ->andFilterWhere(['like', 'order_code', $this->order_code])
                ->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'customer_email', $this->customer_email])
                ->andFilterWhere(['like', 'customer_phone', $this->customer_phone]);
        }

        return $dataProvider;
    }
}
